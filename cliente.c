
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>


int main(int argc, char *argv[]){
	int socketfd;

        if(argc == 1){
                printf("Escribir ./server <interfaz> <numero de puerto>\n");
                exit(-1);

        }

        if(argc != 3){
                printf("Especificar un numero de puerto e interfaz\n");


        }

        int puerto = atoi(argv[2]);


        struct sockaddr_in direccion_destino;

        memset(&direccion_destino, 0, sizeof(direccion_destino));

        direccion_destino.sin_sin_family = AF_INET;
        direccion_destino.sin_port = htons(puerto);
        direccion_destino.sin_addr.s_addr = inet_addr(argv[1]);

        int fd;
        int err = 0;

	if((fd = socket(direccion_destino.sin_family, SOCK_STREAM, 0)) < 0){
                perror("Error al crear el socket\n");
                return -1;
        }

	int res = connect(fd, (struct sockaddr *)&direccion_destino, sizeof(direccion_destino));

	if(res<0){
		close(fd);
		perror("Error al conectar\n");
		return -1;
	}

	int env = write(fd, argv[3], 100);

	char buf3[8000000] = {0};
	int recibido = 0;

	umask(0);

	while( (recibido = read(fd, buf3, 8000000)) > 0){
		int open3 = open(argv[4], O_WRONLY | O_CREAT | O_TRUNC, 0777);
		if (open3<0){
			perror("Error al abrir el archivo\n");
		}

		else{
			write(open3, buf3, 8000000);
		}

		close(open3);

	}
	close(fd);


}
