all: bin/server bin/cliente

bin/server: obj/server.o
	gcc obj/server.o -o bin/server

bin/cliente: obj/cliente.o
	gcc obj/cliente.o -o bin/cliente

obj/server.o: src/server.c
	gcc -c src/server.c -o obj/server.o

obj/cliente.o: src/cliente.c
	gcc -c src/cliente.c -o obj/cliente.o

clean:
	clear
