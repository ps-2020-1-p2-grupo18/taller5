
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>



int main(int argc, char *argv[]){
	int socketfd;

	if(argc == 1){
		printf("Escribir ./server <interfaz> <numero de puerto>\n");
		exit(-1);

	}

	if(argc != 3){
		printf("Especificar un numero de puerto e interfaz\n");

	
	}

	int puerto = atoi(argv[2]);


	struct sockaddr_in direccion_servidor;

	memset(&direccion_servidor, 0, sizeof(direccion_servidor));

	direccion_servidor.sin_sin_family = AF_INET;
	direccion_servidor.sin_port = htons(puerto);
	direccion_servidor.sin_addr.s_addr = inet_addr(argv[1]);

	int fd;
	int err = 0;

	if((fd = socket(direccion_servidor.sin_family, SOCK_STREAM, 0)) < 0){
		perror("Error al crear el socket\n");
		return -1;
	}

	if(bind(fd, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor)) < 0){
		perror("Error en bind\n");
		return -1;
	
	
	}

	if(listen(fd, 50) < 0){
		perror("Error en listen\n");
		return -1;
	}

	while(1){
		int sockfd_conectado = accept(fd, NULL, 0);
		printf("Se conecto alguien\n");

		char buf1[800] = {0};
		char buf2[8000000] = {0};

		umask(0);

		int leidos = read(sockfd_conectado, buf1, 800);

		if(leidos < 0){
			perror("Error al leer\n");

		}

		else{
			printf("Recepcion: %s\n", buf1);
			int open1 = open(buf1, O_RDONLY, 0777);
			if (open1<0){
				perror("Error al abrir el archivo\n");
			}

			else{
				write(open1, buf2, 8000000);

			}

			close(open1);
		}
		close(sockfd_conectado);


}


